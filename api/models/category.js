const mongoose = require('mongoose');

const Category = mongoose.model('Category', {
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
});

module.exports = Category;
