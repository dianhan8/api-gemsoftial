const mongoose = require('mongoose');

const CardIdentity = mongoose.model('CardIdentity', {
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  phoneNumber: { type: String, unique: true },
  alias: String,
  thumbnail: String,
  thumbnailUrl: String,
})

module.exports = CardIdentity;
