const mongoose = require('mongoose');

const Post = mongoose.model('Post', {
  _id: mongoose.Schema.Types.ObjectId,
  title: { type: String, required: true, maxLength: 100 },
  description: { type: String, maxLength: 500 },
  content: { type: String, text: true },
  thumbnail: String,
  tag: String,
  category: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
  author: [{ type: mongoose.Schema.Types.ObjectId, ref: 'CardIdentity' }],
});

module.exports = Post;
