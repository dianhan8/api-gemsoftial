const mongoose = require('mongoose');

const Comments = mongoose.model('Comments', {
  _id: mongoose.Schema.Types.ObjectId,
  idfNumber: [{ type: mongoose.Schema.Types.ObjectId, ref: 'CardIdentity' }],
  content: { type: String, text: 'true', maxLength: 300 },
});

module.exports = Comments;