const mongoose = require('mongoose');

const UsersIdentity = mongoose.model('UsersIdentity', {
  _id: mongoose.Schema.Types.ObjectId,
  idfNumber: [{ type: mongoose.Schema.Types.ObjectId, ref: 'CardIdentity' }],
  password: { type: String, maxLength: 30, minLength: 8, unique: true },
  confirmed: { type: Boolean, default: false },
  blocked: { type: Boolean, default: false },
  timeBlocked: { type: Date },
});

module.exports = UsersIdentity;
