const express = require('express');
const router = express.Router();
const multer = require('multer');

const Controller = require('./../controller/post');

const storage = multer.diskStorage({
  destination: (req, res, cb) => {
    cb(null, '/storages/post');
  },
  filename: (req, res, cb) => {
    cb(null, file.fieldname + '_' + new Date.now())
  }
})

const upload = multer({ storage: storage });

router.get('/', Controller.postGetAll);

router.post('/', upload.single('thumbnail'), Controller.postInsert);

module.exports = router;
