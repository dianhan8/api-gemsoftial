const express = require('express');
const router = express.Router();
const Controller = require('./../controller/indentity');

router.get('/', Controller.IdentityGetAll);
router.get('/:id', Controller.IdentityGetById);
router.post('/', Controller.IdentityPost);
router.patch('/:id', Controller.IdentityGetById);
router.delete('/:id', Controller.IdentityDelete);

module.exports = router;