const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).json({
      message: 'Handling Get requets'
    });
});

router.post('/', (req, res, next) => {
    res.status(200).json({
        message: 'Handling Post requests'
    });
});


module.exports = router;
