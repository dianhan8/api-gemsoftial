const mongoose = require('mongoose');
const sgMail = require('@sendgrid/mail');
const CardIdentity = require('./../models/cardIdentity');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.IdentityGetAll = async (req, res, next) => {

  const { page, perPage } = req.query;
  const limit = Math.abs(perPage) || 10;
  const toPage = (Math.abs(page) || 1) - 1;

  try {

    CardIdentity.find().limit(limit).skip(limit * toPage)
      .exec(function (err, cardIdentity){

          if(err){
            new Error(err);
          }

          CardIdentity.estimatedDocumentCount()
            .exec(function (err, count){
              
              const pages = count < perPage ? 1 : count / perPage;
              const nextPage = pages > 1 ? page + 1 : null;
              const prevPage = page - 1;
              const nextUrl = nextPage ? 'http://localhost:8000/identity?perPage='+ perPage +'&page=' + nextPage : null;
              const prevUrl = page > 1 ? 'http://localhost:8080/identity?perPage='+ perPage +'&page=' + prevPage : null;
       
              return res.status(200).json({
                currentPage: page,
                prevUrl: prevUrl,
                data: cardIdentity.map(item => {

                    return {
                      id: item._id,
                      name: item.name,
                      alias: item.alias,
                      email: item.email,
                      phoneNumber: item.phoneNumber,
                    }

                }),
                nextPage: nextPage,
                nextUrl: nextUrl,
                perPage: perPage,
                count: count,
                pages: pages
              });

            });

      });

  } catch (error) {

    return res.status(500).json({
      message: 'failed query data with errors',
      error: error,
    });

  }
}

exports.IdentityGetById = (req, res, next) => {
  res.status(200).json({
    message: 'fecth data by id',
  })
}

exports.IdentityPost = async  (req, res, next) => {
  
  const { name, email, alias, phoneNumber } = req.body;
 
  try {

    const findUser = await CardIdentity.find({ email, phoneNumber }).exec();
    
    if(findUser.length < 1){

      const identity = new CardIdentity({
        _id: mongoose.Types.ObjectId(),
        name,
        email,
        alias,
        phoneNumber,
      });

      identity.save();
      
      // Send Email with Sendgrid
      const msg = {
        to: 'dianhan8@gmail.com',
        from: 'support@denect.net',
        subject: 'Sending with Twilio SendGrid is Fun',
        text: 'and easy to do anywhere, even with Node.js',
        html: '<strong>and easy to do anywhere, even with Node.js</strong>',
      };

      sgMail.send(msg)

      return res.status(200).json({
        message: 'Identity been created',
        identity: identity,
      });

    } else {

      return res.status(409).json({
        message: 'user has been already register',
      });

    }

  } catch (error) {
      
      return res.status(500).json({
        message: 'failed execute code',
        error,
      });

  }

}

exports.IdentityUpdate = (req, res, next) => {
  const { name, email, alias, phoneNumber } = req.body;
}

exports.IdentityDelete = (req, res, next) => {

}
