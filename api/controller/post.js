const Post = require('./../models/post');

exports.postGetAll = (req, res, next) => {
  const { page, perPage } = req.query;
  const configPage = Math.max(0, page);
  try {
    Post.find()
      .limit(perPage)
      .skip(configPage * page)
      .sort({ name: 'asc' })
      .exec(function (err, posts){
        Post.count()
          .exec(function (err, count){
            res.status(200).json({
              count: count,
              data: posts,
              page: page,
              pages: count / perPage
            });
          });
      });
  } catch (error) {
    res.status(500).json({
      message: 'failed query data with errors',
      error: error,
    });
  }
}

exports.postGetById = (req, res, next) => {
  const { postId } = req.params;
  try {

  } catch (error) {

  }
}

exports.postInsert = (req, res, next) => {
  const { title, description, category, tag, content } = req.body;
  const fileThumbnail = req.file;
  try {

  } catch (error) {

  }
}

exports.postUpdate = (req, res, next) => {
  const { postId } = req.param;
  try {

  } catch (error) {

  }
}

exports.postDelete = (req, res, next) => {
 const { postId } = req.param;
 try {

 } catch (error) {

 }
}